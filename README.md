# Cyber-in-Saclay-Tamarin


This repository contains materials for the Tamarin course as part of the "Cyber in Saclay" Winter School in Cybersecurity.

See the [Tamarin Manual](https://tamarin-prover.github.io/manual/) for [installation instructions](https://tamarin-prover.github.io/manual/book/002_installation.html).

Go to the [`exercises` folder](https://gitlab.inria.fr/jdreier/cyber-in-saclay-tamarin/-/tree/master/exercises) for the exercise material.

# Virtual Machine

A ready-to-use virtual machine (to be used, e.g., with VirtualBox) with Ubuntu and Tamarin pre-installed is available [here](https://filesender.renater.fr/?s=download&token=6da46793-7073-46ff-ab64-7290a77c6237) (you can check that the sha256 sum is  `5ba341f9ade0b50862286a4613cc85a1720ce4ad881eddba1d6d9b4e3787a8e8`).

It requires approximately 15 GB of disk space to run. To log in, use the user `tamarin` with password `tamarin`.

The exercise folder is already checked out locally and can be found in `~/cyber-in-saclay-tamarin/exercises/`, there is also a shortcut on the desktop.

**_Before starting, please open a terminal in this folder and enter `git pull` to get the latest version of the exercise files !_**
