/*********************************************************************************/
/*              Cyber in Saclay == Winter School in Cybersecurity                */
/*                          Tamarin Tutorial                                     */
/*                       J. Dreier & L. Hirschi                                  */
/*                             Exercise 1                                        */
/*********************************************************************************/

# Exercise 1
We will model and analyze variants of the Feldhofer protocol [1,2], which has been
proposed to secure RFID tags.

A first variant of this protocol is already modeled in `ex1.spthy`. The goal of this
exercise is to analyze this protocol and propose improvements that are provably
secure.

### Protocol V1
`ex1.spthy` models a very simple two-party authentication protocol. It assumes two
agents (say A acting as initiator and B acting as responder) that share a
symmetric key `k`, called pre-shared key (PSK). Each pair of shares a PSK.
The protocol is specified by the following message sequence chart:
``` A                       B   (A and B initially share a symmetric secret key k)
    | --------  X --------> |
    | <-- enc_k(<X,Y>) ---- |
    | ------- OK ---------> |
    !                       !
```
where `X` and `Y` are nonces, `k` is the PSK, and `enc_k()` denotes the symmetric
encryption with k.

### Load and check the model
To load the model, type in a terminal:
```
~/.local/bin/tamarin-prover interactive --auto-sources <path>
```
here `<path>` is the path to the model. Note the use of the option `--auto-sources`. (You will understand what it is and what it does after the lecture on Wednesday.)
You should obtain an output of the form
```
GraphViz tool: 'dot'
 checking version: dot - graphviz version 2.40.1 (20161225.0304). OK.
maude tool: 'maude'
 checking version: 2.7.1. OK.
 checking installation: OK.
The server is starting up on port 3001.
Browse to http://127.0.0.1:3001 once the server is ready.

Loading the security protocol theories './*.spthy' ...
Finished loading theories ... server ready at

    http://127.0.0.1:3001
```
Open the link given at the end (here `http://127.0.0.1:3001`) in a browser. Click on your model that should appear in the list.
You can now select a lemma and click `'a'` to launch the prover.

You can also request Tamarin to try to autoprove all the lemmas in a file as follows:
```
~/.local/bin/tamarin-prover --prove --auto-sources <file>

```


### Questions
 - Question 1: Load and verify the model `ex1.spthy`. Interpret the results. Encode secrecy of Y (add an event and write the lemma formula). Re-check the model.
 - Question 2: Propose a fix that remedies the authentication problem B->A, encode it and check it.  [Hint: modify the last message from A to B that also encrypts both nonces. Try with `senc(<X,Y>,~k)`, re-check, comment on the results, and propose a fix.] [Hint2: If Tamarin has trouble proving your final fix (very long computation or or non-termination), please ask for help. ]
 - Question 3: Comment on why we do not produce the fact `!Ltk( $B, $A, ~k)` in the `generate_PSK` rule (see line 34) (check the model with this extra fact and interpret the results).
 - Question 4: Encode injective-agreement properties (for both directions) and re-check the model. Replace the encrypted nonce(s) in the last message by constants (e.g., `'OK'`), re-check, and interpret the results.
 - Question 5: Replace the uses of symmetric encryption by asymmetric encryptions. Suppose that all agents' public keys are indeed public and known to all other agents, in particular by the attacker. Comment on the analysis results and propose fixes. [Hint: you can encrypt other messages.]

## Other exercises
If you are done with this first exercise, we encourage you to try out other exercices designed by colleagues [here](https://github.com/benjaminkiesl/tamarin_toy_protocol) and [there](https://github.com/aseemr/Indocrypt-VerifiedCrypto-Tutorials/tree/main/Tamarin).

## Sources
[1] M. Feldhofer, S. Dominikus, and J. Wolkerstorfer. Strong authentication for RFID systems using the AESalgorithm. InCryptographic Hardware and Embedded Systems-CHES 2004, pages 357–370. Springer, 2004
https://www.iacr.org/archive/ches2004/31560357/31560357.pdf

[2] Hirschi, Lucca, David Baelde, and Stéphanie Delaune. "A method for unbounded verification of privacy-type properties." Journal of Computer Security 27.3 (2019): 277-342.
https://arxiv.org/pdf/1710.02049.pdf
